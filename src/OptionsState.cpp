#include "OptionsState.h"
#include "Game.h"
#include "Resources.h"
#include "InputHandler.h"

#define FONT "../font/game_boy.ttf"

OptionsState::OptionsState() :
  bg_("../img/options/options.png", 1, 0) {

  options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "SCALE", Colors::brown, 18, 42));
	options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "FULLSCREEN", Colors::brown, 18, 52));
	options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "VSYNC", Colors::brown, 18, 62));
	options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "SOUND", Colors::brown, 18, 72));
  options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "MUSIC", Colors::brown, 18, 82));
  options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "BACK", Colors::brown, 18, 92));

  optionsValues_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::SCALE), Colors::brown, 118, 42));
  optionsValues_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, Resources::FULLSCREEN == 0 ? "OFF" : "ON", Colors::brown, 118, 52));
  optionsValues_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, Resources::VSYNC == 0 ? "OFF" : "ON", Colors::brown, 118, 62));
  optionsValues_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::SOUND_VOLUME), Colors::brown, 118, 72));
  optionsValues_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::MUSIC_VOLUME), Colors::brown, 118, 82));

  currentSelectedOption_ = 0;
}

OptionsState::~OptionsState() {

}

void OptionsState::update(float dt){
  InputHandler& input = InputHandler::getInstance();
  input.update();

  if (input.quitRequested() || input.keyPress(ESCAPE_KEY)) {
    quit_ = true;
  }

  // down
  if(input.keyPress(DOWN_ARROW_KEY) || input.keyPress(S_KEY) || input.gamePadPress(GAMEPAD_ARROW_DOWN)) {
    if(currentSelectedOption_ != (int)options_.size()-1) {
      currentSelectedOption_++;
    }
  }

  // up
  if(input.keyPress(UP_ARROW_KEY) || input.keyPress(W_KEY) || input.gamePadPress(GAMEPAD_ARROW_UP)) {
    if(currentSelectedOption_ != 0) {
      currentSelectedOption_--;
    }
  }

  // left
  if(input.keyPress(LEFT_ARROW_KEY) || input.keyPress(A_KEY) || input.gamePadPress(GAMEPAD_ARROW_LEFT)) {
    switch(currentSelectedOption_) {
      case 0:
        if (Resources::SCALE > 1) {
          Resources::SCALE--;
          optionsValues_.at(0) = new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::SCALE), Colors::brown, 118, 42);
          Game::getInstance().changeResolution(Resources::WINDOW_WIDTH * Resources::SCALE, Resources::WINDOW_HEIGHT * Resources::SCALE);
        }
        break;
      case 1:
        Resources::FULLSCREEN = 0;
        optionsValues_.at(1) = new Text(FONT, 10, Text::TextStyle::SOLID, "OFF", Colors::brown, 118, 52);
        Game::getInstance().setFullscreen(false);
        break;
      case 2:
        Resources::VSYNC = 0;
        optionsValues_.at(2) = new Text(FONT, 10, Text::TextStyle::SOLID, "OFF", Colors::brown, 118, 62);
        Game::getInstance().setVsync(false);
        break;
      case 3:
        if (Resources::SOUND_VOLUME > 1) {
            Resources::SOUND_VOLUME--;
            optionsValues_.at(3) = new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::SOUND_VOLUME), Colors::brown, 118, 72);

        }
        break;
      case 4:
        if (Resources::MUSIC_VOLUME > 1) {
            Resources::MUSIC_VOLUME--;
            optionsValues_.at(4) = new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::MUSIC_VOLUME), Colors::brown, 118, 82);

        }
        break;
      default:        
        break;
    }
  }

  // right
  if(input.keyPress(RIGHT_ARROW_KEY) || input.keyPress(D_KEY) || input.gamePadPress(GAMEPAD_ARROW_RIGHT)) {
    switch(currentSelectedOption_) {
      case 0:
        if (Resources::SCALE < 10) {
          Resources::SCALE++;
          optionsValues_.at(0) = new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::SCALE), Colors::brown, 118, 42);
          Game::getInstance().changeResolution(Resources::WINDOW_WIDTH * Resources::SCALE, Resources::WINDOW_HEIGHT * Resources::SCALE);
        }
        break;
      case 1:
        Resources::FULLSCREEN = 1;
        optionsValues_.at(1) = new Text(FONT, 10, Text::TextStyle::SOLID, "ON", Colors::brown, 118, 52);
        Game::getInstance().setFullscreen(true);
        break;
      case 2:
        Resources::VSYNC = 1;
        optionsValues_.at(2) = new Text(FONT, 10, Text::TextStyle::SOLID, "ON", Colors::brown, 118, 62);
        Game::getInstance().setVsync(true);
        break;
      case 3:
        if (Resources::SOUND_VOLUME < 10) {
            Resources::SOUND_VOLUME++;
            optionsValues_.at(3) = new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::SOUND_VOLUME), Colors::brown, 118, 72);

        }
        break;
      case 4:
        if (Resources::MUSIC_VOLUME < 10) {
            Resources::MUSIC_VOLUME++;
            optionsValues_.at(4) = new Text(FONT, 10, Text::TextStyle::SOLID, std::to_string(Resources::MUSIC_VOLUME), Colors::brown, 118, 82);

        }
        break;
      default:
        break;
    }
  }

  // space
  if(input.keyPress(SPACE_BAR) || input.keyPress(ENTER) || input.gamePadPress(GAMEPAD_A)) {
    if(currentSelectedOption_ != 0) {
      switch(currentSelectedOption_) {
        case 5:
          pop_ = true;
          break;
        default:
          break;
      }
    }
  }

}

void OptionsState::render(){
  bg_.render();

  for(int i=0; i<options_.size(); i++) {
    if(i == currentSelectedOption_) {
      options_[i]->setColor(Colors::white);
    } else {
      options_[i]->setColor(Colors::brown);
    }
      options_[i]->render();
  }

  for(int i=0; i<optionsValues_.size(); i++) {
    if(i == currentSelectedOption_) {
      optionsValues_[i]->setColor(Colors::white);
    } else {
      optionsValues_[i]->setColor(Colors::brown);
    }
      optionsValues_[i]->render();
  }
}

void OptionsState::pause(){

}

void OptionsState::resume(){

}
