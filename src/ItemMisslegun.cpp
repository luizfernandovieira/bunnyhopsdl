#include "ItemMisslegun.h"
#include "Camera.h"

ItemMisslegun::ItemMisslegun(int x, int y) :
    sp_("../img/item/misslegun.png", 1, 0.1) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

ItemMisslegun::~ItemMisslegun() {

}

void ItemMisslegun::update(float dt) {

}

void ItemMisslegun::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void ItemMisslegun::notifyCollision(GameObject& object) {

}

bool ItemMisslegun::is(const std::string& type) {
  return false;
}

bool ItemMisslegun::isDead() {
  return false;
}
