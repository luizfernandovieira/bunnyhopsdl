#include "Sound.h"
#include "Resources.h"

Sound::Sound() : chunk(nullptr) {

}

Sound::Sound(const std::string& file) {    
    open(file);
}

void Sound::play (int times) {
    if (isOpen()) {
        channel = Mix_PlayChannel(-1, chunk.get(), times);
        if(channel == -1) {
          printf("Play chunk: %s\n", Mix_GetError());
          exit(-1);
        }
    }
}

void Sound::stop () {
    Mix_HaltChannel(channel);
}

void Sound::open (const std::string& file) {
  chunk = Resources::getSound(file);
}

bool Sound::isOpen () {
    return (chunk != nullptr);
}
