#include "BulletPlayerPistol.h"
#include "Camera.h"

BulletPlayerPistol::BulletPlayerPistol(int x, int y) :
  sp_("../img/bullets/bullet_player_pistol.png"),
  moveSpeed_(50) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

BulletPlayerPistol::~BulletPlayerPistol() {

}

void BulletPlayerPistol::update(float dt) {

}

void BulletPlayerPistol::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void BulletPlayerPistol::notifyCollision(GameObject & object) {

}

bool BulletPlayerPistol::is(const std::string& type) {
  return false;
}

bool BulletPlayerPistol::isDead() {
  return false;
}
