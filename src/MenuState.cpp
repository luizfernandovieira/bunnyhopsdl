#include "MenuState.h"
#include "Game.h"
#include "Resources.h"
#include "InputHandler.h"
#include "GameStatePhase1.h"
#include "GameStatePhase2.h"
#include "OptionsState.h"

#define FONT "../font/game_boy.ttf"

MenuState::MenuState() :
  bg_("../img/menu/menu.png", 1, 0) {

  if(!Mix_PlayingMusic()) {
    Mix_VolumeMusic(0);
  }

  for(int i=0; i<16; i++) {
      Mix_Volume(i, Resources::SOUND_VOLUME * 16);
  }

  options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "PLAY", Colors::brown, 53, 86));
	options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "OPTIONS", Colors::brown, 53, 100));
	options_.push_back(new Text(FONT, 10, Text::TextStyle::SOLID, "EXIT", Colors::brown, 53, 112));

  currentSelectedOption_ = 0;
}

MenuState::~MenuState() {

}

void MenuState::update(float dt){
  InputHandler& input = InputHandler::getInstance();
  input.update();

  if (input.quitRequested() || input.keyPress(ESCAPE_KEY)) {
    quit_ = true;
  }

  // down
  if(input.keyPress(DOWN_ARROW_KEY) || input.keyPress(S_KEY) || input.gamePadPress(GAMEPAD_ARROW_DOWN)) {
    if(currentSelectedOption_ != (int)options_.size()-1) {
      currentSelectedOption_++;
    }
  }

  // up
  if(input.keyPress(UP_ARROW_KEY) || input.keyPress(W_KEY) || input.gamePadPress(GAMEPAD_ARROW_UP)) {
    if(currentSelectedOption_ != 0) {
      currentSelectedOption_--;
    }
  }

  // space or enter or gamepad A
  if(input.keyPress(SPACE_BAR) || input.keyPress(ENTER) || input.gamePadPress(GAMEPAD_A)) {
    switch(currentSelectedOption_) {
      case 0:
        Game::getInstance().push(new GameStatePhase2());
        break;
      case 1:
        Game::getInstance().push(new OptionsState());
        break;
      case 2:
        quit_ = true;
        break;
    }
  }

}

void MenuState::render(){
  bg_.render();

  for(int i=0; i<options_.size(); i++) {
    if(i == currentSelectedOption_) {
      options_[i]->setColor(Colors::white);
    } else {
      options_[i]->setColor(Colors::brown);
    }
      options_[i]->render();
  }
}

void MenuState::pause(){

}

void MenuState::resume(){

}
