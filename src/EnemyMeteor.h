#ifndef ENEMY_METEOR_H
#define ENEMY_METEOR_H

#include "GameObject.h"
#include "Sprite.h"

enum EnemyMeteorSize {
    SMALL,
    MEDIUM,
    LARGE
};

class EnemyMeteor : public GameObject {

public:
    EnemyMeteor(int x = 0, int y = 0, EnemyMeteorSize size = EnemyMeteorSize::SMALL);
    ~EnemyMeteor();
    void update(float dt);
    void render();
    void notifyCollision(GameObject& object);
    bool is(const std::string& type);
    bool isDead();

private:
    Sprite sp_;

};

#endif
