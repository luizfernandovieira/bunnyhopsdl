 #include "EnemyBomb.h"
 #include "Camera.h"
 #include "Game.h"

 EnemyBomb::EnemyBomb(int x, int y) :
   sp_("../img/entity/enemies/bomb.png", 4, 0.1),
   state_(EnemyBombState::FLYING) {
   box_.x(x);
   box_.y(y);
   box_.w(sp_.getWidth());
   box_.h(sp_.getHeight());
 }

 EnemyBomb::~EnemyBomb() {

 }

 void EnemyBomb::update(float dt) {

 }

 void EnemyBomb::render() {

   switch (state_) {
     case EnemyBombState::FLYING:
       sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
       break;
   }

 }

 void EnemyBomb::notifyCollision(GameObject& object) {

 }

 bool EnemyBomb::is(const std::string& type) {
   return false;
 }

 bool EnemyBomb::isDead() {
   return false;
 }
