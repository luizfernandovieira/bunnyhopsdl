#include "Sprite.h"

#include <iostream>

#include "Game.h"
#include "Resources.h"

Sprite::Sprite() :
  texture(nullptr),
  width(0),
  height(0),
  scaleX(1),
  scaleY(1),
  frameCount(1),
  currentFrame(0),
  timeElapsed(0),
  frameTime(0) {
}

Sprite::Sprite(const std::string& file, int frameCount, float frameTime) :
  texture(nullptr),
  scaleX(1),
  scaleY(1),
  frameCount(frameCount),
  currentFrame(0),
  timeElapsed(0),
  frameTime(frameTime) {
  open(file);
}

Sprite::~Sprite() {

}

void Sprite::open(const std::string& file) {
  width = height = 0;
  texture = Resources::getImage(file);

  if (texture != nullptr) {
      SDL_QueryTexture(texture.get(), nullptr, nullptr, &width, &height);
  }

  setClip(0, 0, width/frameCount, height);
}

void Sprite::setClip(int x, int y, int w, int h) {
  clipRect.x = x;
  clipRect.y = y;
  clipRect.w = w;
  clipRect.h = h;
}

void Sprite::render(int x, int y, float angle) const {
    if (texture == nullptr) {
      return;
    }

    SDL_Rect dst;
    dst.x = x;
    dst.y = y;
    dst.w = (int) (clipRect.w * scaleX);
    dst.h = (int) (clipRect.h * scaleY);

    SDL_RenderCopyEx(Game::getInstance().getRenderer(),
                     texture.get(), &clipRect, &dst,
                     angle * 180/PI, nullptr, SDL_FLIP_NONE);
}

int Sprite::getWidth() const {
    return (int) (width * scaleX / frameCount);
}

int Sprite::getHeight() const {
    return (int) (height * scaleY);
}

void Sprite::setScaleX(float scale) {
    scaleX = scale;
}

void Sprite::setScaleY(float scale) {
    scaleY = scale;
}

bool Sprite::isOpen() const {
    return texture != nullptr;
}

void Sprite::update(float dt) {

    timeElapsed += dt;

    if (timeElapsed > frameTime) {

        timeElapsed -= frameTime;
        ++currentFrame;

        if (currentFrame == frameCount) {
            currentFrame = 0;
        }
        setClip((width/frameCount) * currentFrame,
                 0,
                 width/frameCount,
                 height);
    }
}

void Sprite::setFrame(int frame) {
    currentFrame = frame;
    timeElapsed = 0;
    setClip((width/frameCount) * currentFrame,
            0,
            width/frameCount,
            height);
}

void Sprite::setFrameCount(int frameCount) {
    this->frameCount = frameCount;
    setFrame(currentFrame); // para refazer o clip
}

void Sprite::setFrameTime(float frameTime) {
    this->frameTime = frameTime;
}

SDL_Texture* Sprite::getTexture() {
  return texture.get();
}
