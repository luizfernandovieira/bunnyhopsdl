#ifndef SPLASH_STATE_H
#define SPLASH_STATE_H

#include <vector>

#include "State.h"
#include "Sprite.h"
#include "Timer.h"

class SplashState : public State {

public:
	SplashState();
	~SplashState();
	void update(float dt);
	void render();
	void pause();
	void resume();

private:
  Sprite* splashImage_;
  Timer timer_;

};

#endif
