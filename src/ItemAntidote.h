#ifndef ITEM_ANTIDOTE_H
#define ITEM_ANTIDOTE_H

#include "IItem.h"
#include "Sprite.h"

class ItemAntidote : public IItem {

public:
    ItemAntidote(int x = 0, int y = 0);
    ~ItemAntidote();
    void update(float dt) override;
    void render() override;
    void notifyCollision(GameObject& object) override;
    bool is(const std::string& type) override;
    bool isDead() override;

protected:
    Sprite sp_;

};

#endif
