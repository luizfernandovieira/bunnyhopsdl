#pragma once

class Timer {

public:
  Timer();
  void update(float dt);
  void restart();
  float get() const;

private:
  float time_;

};
