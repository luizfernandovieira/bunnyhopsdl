#include "GameStatePhase2.h"
#include "Game.h"
#include "Resources.h"
#include "InputHandler.h"
#include "EnemyBomb.h"
#include "EnemyMeteor.h"
#include "ItemCoin.h"
#include "Camera.h"

#define FONT "../font/game_boy.ttf"

GameStatePhase2::GameStatePhase2() :
  bg_("../img/bg.png", 1, 0),
  hud_("../img/hud/hud.png", 1, 0) {

  player_ = new PlayerShip(50, 50);
  map_ = new Tmx::Map();

  stageHud_ = new Text(FONT, 5, Text::TextStyle::SOLID, "STAGE:", Colors::beige, 20, 2);
  lifeHud_ = new Text(FONT, 5, Text::TextStyle::SOLID, "LIFE:", Colors::beige, 20, 10);
  bulletsHud_ = new Text(FONT, 5, Text::TextStyle::SOLID, "BULLETS:", Colors::beige, 88, 2);
  weaponHud_ = new Text(FONT, 5, Text::TextStyle::SOLID, "WEAPON:", Colors::beige, 88, 10);

  stageHudValue_ = new Text(FONT, 5, Text::TextStyle::SOLID, "01", Colors::beige, 54, 2);
  lifeHudValue_ = new Text(FONT, 5, Text::TextStyle::SOLID, "3", Colors::beige, 54, 10);
  bulletsHudValue_ = new Text(FONT, 5, Text::TextStyle::SOLID, "100", Colors::beige, 128, 2);
  weaponHudValue_ = new Text(FONT, 5, Text::TextStyle::SOLID, "PSTL", Colors::beige, 128, 10);

  // Camera::startSpaceShooter();
  // Camera::startMoving();

  create();
}

GameStatePhase2::~GameStatePhase2() {
  objects.clear();
  delete map_;
}

void GameStatePhase2::create() {
  std::string fileName = "../map/phase_2.tmx";
  map_->ParseFile(fileName);

  if (map_->HasError()) {
    printf("error code: %d\n", map_->GetErrorCode());
    printf("error text: %s\n", map_->GetErrorText().c_str());
    map_->GetErrorCode();
  }

  printf("====================================\n");
  printf("Map\n");
  printf("====================================\n");

  printf("Version: %1.1f\n", map_->GetVersion());
  printf("Version: %1.1f\n", map_->GetVersion());
  printf("Orientation: %d\n", map_->GetOrientation());

  if (!map_->GetBackgroundColor().IsTransparent()) {
    printf("Background Color (hex): %s\n", map_->GetBackgroundColor().ToString().c_str());
  }

  printf("Render Order: %d\n", map_->GetRenderOrder());

  if (map_->GetStaggerAxis()) {
    printf("Stagger Axis: %d\n", map_->GetStaggerAxis());
  }

  if (map_->GetStaggerIndex()) {
    printf("Stagger Index: %d\n", map_->GetStaggerIndex());
  }

  printf("Width: %d\n", map_->GetWidth());
  printf("Height: %d\n", map_->GetHeight());
  printf("Tile Width: %d\n", map_->GetTileWidth());
  printf("Tile Height: %d\n", map_->GetTileHeight());

  const std::unordered_map<std::string, Tmx::Property> &mapProperties = map_->GetProperties().GetPropertyMap();
  for (auto &pair : mapProperties) {
    const Tmx::Property &property = pair.second;
    std::string type;
    if (property.GetType() == Tmx::TMX_PROPERTY_STRING) {
      type = "String";
    } else if (property.GetType() == Tmx::TMX_PROPERTY_FLOAT) {
      type = "Float";
    } else if (property.GetType() == Tmx::TMX_PROPERTY_INT) {
      type = "Integer";
    } else if (property.GetType() == Tmx::TMX_PROPERTY_BOOL) {
      type = "Boolean";
    } else if (property.GetType() == Tmx::TMX_PROPERTY_COLOR) {
      type = "Color";
    } else if (property.GetType() == Tmx::TMX_PROPERTY_FILE) {
      type = "File";
    } else {
      type = "Unknown";
    }
    printf("Map property %s (%s) = %s\n", pair.first.c_str(), type.c_str(),  property.GetValue().c_str());
  }

  // Iterate through the tile layers.
  for (int i = 0; i < map_->GetNumTileLayers(); ++i) {
    printf("                                    \n");
    printf("====================================\n");
    printf("Layer : %02d/%s \n", i, map_->GetTileLayer(i)->GetName().c_str());
    printf("====================================\n");
    // Get a layer.
    const Tmx::TileLayer *tileLayer = map_->GetTileLayer(i);
    for (int y = 0; y < tileLayer->GetHeight(); ++y) {
      for (int x = 0; x < tileLayer->GetWidth(); ++x) {
        if (tileLayer->GetTileTilesetIndex(x, y) == -1) {
          printf("........    ");
        } else {
          // Get the tile's id and gid.
          printf("%03d(%03d)", tileLayer->GetTileId(x, y), tileLayer->GetTileGid(x, y));
          // Find a tileset for that id.
          //const Tmx::Tileset *tileset = map->FindTileset(layer->GetTileId(x, y));
          if (tileLayer->IsTileFlippedHorizontally(x, y)) {
            printf("h");
          } else {
            printf(" ");
          }
          if (tileLayer->IsTileFlippedVertically(x, y))
          {
            printf("v");
          } else {
            printf(" ");
          }
          if (tileLayer->IsTileFlippedDiagonally(x, y)) {
            printf("d ");
          } else {
            printf("  ");
          }
        }
      }
      printf("\n");
    }
  }

  printf("\n\n");

  // Iterate through all of the object groups.
  for (int i = 0; i < map_->GetNumObjectGroups(); ++i) {
    printf("                                    \n");
    printf("====================================\n");
    printf("Object group : %02d\n", i);
    printf("====================================\n");

    const Tmx::ObjectGroup *objectGroup = map_->GetObjectGroup(i);

    for (int j = 0; j < objectGroup->GetNumObjects(); ++j) {
      const Tmx::Object *object = objectGroup->GetObject(j);

      printf("Object Name: %s\n", object->GetName().c_str());
      printf("Object Position: (%03d, %03d)\n", object->GetX(), object->GetY());
      printf("Object Size: (%03d, %03d)\n", object->GetWidth(), object->GetHeight());

      if (object->GetGid() != 0) {
        printf("Object(tile) gid: %d\n", object->GetGid());
        printf("Object(tile) type: %s\n", object->GetType().c_str());
      }

      const Tmx::Polygon *polygon = object->GetPolygon();
      if (polygon != 0) {
        for (int i = 0; i < polygon->GetNumPoints(); i++) {
          const Tmx::Point &point = polygon->GetPoint(i);
          printf("Object Polygon: Point %d: (%f, %f)\n", i, point.x, point.y);
        }
      }

      const Tmx::Polyline *polyline = object->GetPolyline();
      if (polyline != 0) {
        for (int i = 0; i < polyline->GetNumPoints(); i++) {
          const Tmx::Point &point = polyline->GetPoint(i);
          printf("Object Polyline: Point %d: (%f, %f)\n", i, point.x, point.y);
        }
      }

      const Tmx::Text *text = object->GetText();
      if(text != 0) {
        printf("--Object Text--\n");
        printf("Font family: %s\n", text->GetFontFamily().c_str());
        printf("Pixel size: %d\n", text->GetPixelSize());
        printf("Wraps: %d\n", text->Wraps());
        printf("Bold: %d, Italic: %d, Underline: %d, Strikeout: %d\n", text->IsBold(), text->IsItalic(), text->IsUnderline(), text->IsStrikeout());
        printf("Kerning: %d\n", text->UseKerning());
        printf("Horizontal ALignment: %d\n", text->GetHorizontalAlignment());
        printf("Vertical Alignment: %d\n", text->GetVerticalAlignment());
        printf("Color: %d, %d, %d, %d", text->GetColor()->GetRed(), text->GetColor()->GetGreen(), text->GetColor()->GetBlue(), text->GetColor()->GetAlpha());
      }

      // Instanciating Objects
      if (object->GetName() == "Bomb") {
        addObject(new EnemyBomb(object->GetX(), object->GetY()));
      }

      if (object->GetName() == "MeteorSm") {
        addObject(new EnemyMeteor(object->GetX(), object->GetY(), EnemyMeteorSize::SMALL));
      }

      if (object->GetName() == "MeteorMd") {
        addObject(new EnemyMeteor(object->GetX(), object->GetY(), EnemyMeteorSize::MEDIUM));
      }

      if (object->GetName() == "MeteorLg") {
        addObject(new EnemyMeteor(object->GetX(), object->GetY(), EnemyMeteorSize::LARGE));
      }

      if (object->GetName() == "Coin") {
        addObject(new ItemCoin(object->GetX(), object->GetY()));
      }
    }
  }
}

void GameStatePhase2::update(float dt){
  Camera::update(dt);
  InputHandler& input = InputHandler::getInstance();

  bg_.update(dt);
  player_->update(dt);

  for (auto& ptr : objects) {
    ptr->update(dt);
  }

  hud_.update(dt);

  if (input.quitRequested() || input.keyPress(ESCAPE_KEY)) {
    quit_ = true;
  }

  updateObjects(dt);
}

void GameStatePhase2::render(){
  bg_.render();
  player_->render();
  renderObjects();
  hud_.render();
  stageHud_->render();
  lifeHud_->render();
  bulletsHud_->render();
  weaponHud_->render();
  stageHudValue_->render();
  lifeHudValue_->render();
  bulletsHudValue_->render();
  weaponHudValue_->render();
}

void GameStatePhase2::pause(){

}

void GameStatePhase2::resume(){

}
