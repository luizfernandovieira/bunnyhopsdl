#ifndef ITEM_MISSLEGUN_H
#define ITEM_MISSLEGUN_H

#include "IItem.h"
#include "Sprite.h"

class ItemMisslegun : public IItem {

public:
    ItemMisslegun(int x = 0, int y = 0);
    ~ItemMisslegun();
    void update(float dt) override;
    void render() override;
    void notifyCollision(GameObject& object) override;
    bool is(const std::string& type) override;
    bool isDead() override;

protected:
    Sprite sp_;

};

#endif
