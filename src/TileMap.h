#pragma once

#include <string>
#include <vector>

#include "TileSet.h"

class TileMap {

  public:
    TileMap(const std::string& file, TileSet* tileSet);
    void load(const std::string& mapPath);
    void setTileSet(TileSet* tileSet);
    int& at(int x, int y, int z = 0);
    void render(int cameraX = 0, int cameraY = 0);
    void renderLayer(int layer, int cameraX = 0, int cameraY = 0);
    int getWidth() const;
    int getHeight() const;
    int getDepth() const;

  private:
    std::vector<int> tileMatrix_;
    TileSet* tileSet_;
    int mapWidth_;
    int mapHeight_;
    int mapDepth_;

};