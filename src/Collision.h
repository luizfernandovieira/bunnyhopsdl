#include <cmath>
#include <algorithm>

#include "Rect.h"

class Collision {
  public:
    static inline bool isColliding(const Rect& a, const Rect& b) {
      if (a.x() < b.x() + b.w() &&
          a.x() + a.w() > b.x() &&
          a.y() < b.y() + b.h() &&
          a.y() + a.h() > b.y()) {
        return true;
      }
      return false;
    }
    static inline float handleCollision(const Rect& a, float velocity, float dt) {
      return 0;
    }
};
