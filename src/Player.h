#ifndef PLAYER_H
#define PLAYER_H

#include <vector>

#include "GameObject.h"
#include "Sprite.h"

class Player : public GameObject {

public:
	Player(int x = 0, int y = 0);
	~Player();
	void update(float dt);
	void render();
  void notifyCollision(GameObject & object);
  bool is(const std::string& type);
  bool isDead();
  void shoot();

private:
  Sprite idleSp_;
  Sprite walkingSp_;
  Sprite jumpingSp_;
  Sprite idleInfectedSp_;
  Sprite walkingInfectedSp_;
  Sprite jumpingInfectedSp_;

};

#endif
