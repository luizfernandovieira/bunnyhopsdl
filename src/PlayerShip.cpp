#include "PlayerShip.h"
#include "Camera.h"
#include "Game.h"
#include "InputHandler.h"
#include "BulletPlayerShip.h"
#include "GameStatePhase2.h"

PlayerShip::PlayerShip(int x, int y) :
  flyingSp_("../img/entity/player_ship/player_ship_flying.png", 6, 0.1),
  dyingSp_("../img/entity/player_ship/player_ship_dying.png", 3, 0.10),
  state_(PlayerShipState::FLYING),
  moveUp_(0),
  moveDown_(0),
  moveRight_(0),
  moveLeft_(0),
  moveHorizontal_(0),
  moveVertical_(0),
  moveSpeed_(50),
  gravity_(1),
  facingRight_(true),
  life_(3),
  canShoot_(true),
  shootCooldown_(0.3),
  shootTimer_(),
  takingDmg_(false),
  canTakeDmg_(true),
  takeDmgCooldown_(1),
  takeDmgTimer_(),
  flicking_(false),
  flickCounter_(3) {
  box_.x(x);
  box_.y(y);
  box_.w(flyingSp_.getWidth());
  box_.h(flyingSp_.getHeight());
}

PlayerShip::~PlayerShip() {

}

void PlayerShip::update(float dt) {
  InputHandler& input = InputHandler::getInstance();
  input.update();

  switch (state_) {

    case FLYING:

      moveDown_= 0;
      moveUp_ = 0;
      moveLeft_ = 0;
      moveRight_ = 0;

      shootTimer_.update(dt);

      if (shootTimer_.get() > shootCooldown_ && !canShoot_) {
        canShoot_ = true;
      }

      if(input.isKeyDown(DOWN_ARROW_KEY) || input.isKeyDown(S_KEY) || input.isGamePadDown(GAMEPAD_ARROW_DOWN)) {
        moveDown_ = 1;
      }

      if(input.isKeyDown(UP_ARROW_KEY) || input.isKeyDown(W_KEY) || input.isGamePadDown(GAMEPAD_ARROW_UP)) {
        moveUp_ = 1;
      }

      if(input.isKeyDown(LEFT_ARROW_KEY) || input.isKeyDown(A_KEY) || input.isGamePadDown(GAMEPAD_ARROW_LEFT)) {
        moveLeft_ = 1;
      }

      if(input.isKeyDown(RIGHT_ARROW_KEY) || input.isKeyDown(D_KEY) || input.isGamePadDown(GAMEPAD_ARROW_RIGHT)) {
        moveRight_ = 1;
      }

      if(input.keyPress(SPACE_BAR) || input.gamePadPress(GAMEPAD_A)) {
        if (canShoot_) {
          canShoot_ = false;
          shootTimer_.restart();
          shoot();
        }
      }

      moveHorizontal_ = moveRight_ - moveLeft_;
      moveVertical_ = moveDown_ - moveUp_;

      box_.x(box_.x() + (moveHorizontal_ * moveSpeed_ * dt));
      box_.y(box_.y() + (moveVertical_ * moveSpeed_ * dt));

      if (life_ <= 0) {
        state_ = PlayerShipState::DYING;
      }

      flyingSp_.update(dt);
      break;

    case DYING:
      dyingSp_.update(dt);
      break;

  }

}

void PlayerShip::render() {

  switch (state_) {
    case FLYING:
      flyingSp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
      break;
    case DYING:
      dyingSp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
      break;
  }

}

void PlayerShip::notifyCollision(GameObject& object) {

}

bool PlayerShip::is(const std::string& type) {
  return false;
}

bool PlayerShip::isDead() {
  return false;
}

void PlayerShip::shoot() {
    BulletPlayerShip* b = new BulletPlayerShip((int) box_.getCenter().x(), (int) box_.getCenter().y());
    Game::getInstance().getCurrentState().addObject(b);
}
