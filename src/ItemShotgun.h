#ifndef ITEM_SHOTGUN_H
#define ITEM_SHOTGUN_H

#include "IItem.h"
#include "Sprite.h"

class ItemShotgun : public IItem {

public:
    ItemShotgun(int x = 0, int y = 0);
    ~ItemShotgun();
    void update(float dt) override;
    void render() override;
    void notifyCollision(GameObject& object) override;
    bool is(const std::string& type) override;
    bool isDead() override;

protected:
    Sprite sp_;

};

#endif
