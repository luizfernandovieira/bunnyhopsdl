#pragma once

#include "Camera.h"
#include "GameObject.h"
#include "Sprite.h"
#include "Timer.h"

class Animation : public GameObject {

public:
  Animation(float x, float y, float rotation,
            const std::string& sprite,
            int frameCount, float frameTime,
            bool ends);
  void update(float dt) override;
  void render() override;
  void notifyCollision(GameObject& other) override;
  bool is(const std::string& type) override;
  bool isDead() override;

private:
  Sprite sp_;
  Timer endTimer_;
  float timeLimit_;
  bool ends_;

};
