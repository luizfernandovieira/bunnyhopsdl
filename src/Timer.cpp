#include "Timer.h"

Timer::Timer() {
    restart();
}

void Timer::update(float dt) {
    time_ += dt;
}

void Timer::restart() {
    time_ = 0;
}

float Timer::get() const {
    return time_;
}
