#pragma once

#include <iostream>
#include <vector>
#include <memory>

#include "State.h"
#include "Player.h"
#include "Tmx.h.in"
#include "Text.h"

class GameStatePhase1 : public State {

public:
    GameStatePhase1();
    ~GameStatePhase1();
    void create();
    void update(float dt);
    void render();
    void pause();
    void resume();

private:
    Player* player_;
    Sprite bg_;
    Tmx::Map* map_;
    Sprite hud_;
    Text* stageHud_;
    Text* lifeHud_;
    Text* bulletsHud_;
    Text* weaponHud_;
    Text* stageHudValue_;
    Text* lifeHudValue_;
    Text* bulletsHudValue_;
    Text* weaponHudValue_;

};