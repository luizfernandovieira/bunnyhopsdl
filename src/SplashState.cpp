#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"
#include "SDL_ttf.h"

#include "SplashState.h"
#include "Game.h"
#include "Resources.h"
#include "InputHandler.h"
#include "MenuState.h"

#define FONT "../font/game_boy.ttf"

SplashState::SplashState() : timer_() {
  splashImage_ = new Sprite("../img/splash/splash_love.png", 1, 0);
}

SplashState::~SplashState() {
  delete splashImage_;
}

void SplashState::update(float dt) {
  InputHandler& input = InputHandler::getInstance();
	input.update();

  timer_.update(dt);

	if (input.quitRequested() || input.keyPress(ESCAPE_KEY)) {
		quit_ = true;
	}

  if(input.keyPress(SPACE_BAR) || input.keyPress(ENTER) || input.gamePadPress(GAMEPAD_A)) {
    Game::getInstance().push(new MenuState());
  }
}

void SplashState::render() {

  if(timer_.get() < 0.5) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 1);
    splashImage_->render();
  } else if (timer_.get() > 0.5 && timer_.get() <= 1) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 16);
    splashImage_->render();
  } else if (timer_.get() > 1 && timer_.get() <= 1.2) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 32);
    splashImage_->render();
  } else if (timer_.get() > 1.2 && timer_.get() <= 1.4) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 64);
    splashImage_->render();
  } else if (timer_.get() > 1.4 && timer_.get() <= 1.6) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 128);
    splashImage_->render();
  } else if (timer_.get() > 1.6 && timer_.get() <= 1.8) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 255);
    splashImage_->render();
  } else if (timer_.get() > 1.8 && timer_.get() <= 2.0) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 64);
    splashImage_->render();
  } else if (timer_.get() > 2.0 && timer_.get() <= 3.0) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 32);
    splashImage_->render();
  } else if (timer_.get() > 3.0 && timer_.get() <= 3.5) {
    SDL_SetTextureAlphaMod(splashImage_->getTexture(), 16);
    splashImage_->render();
  } else {
    Game::getInstance().push(new MenuState());
  }

}

void SplashState::pause() {

}

void SplashState::resume() {

}
