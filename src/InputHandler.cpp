#include "InputHandler.h"
#include "Resources.h"

InputHandler::InputHandler() : inputText() {
	for (int i = 0; i < 6; i++) {
		mouseState[i] = false;
		mouseUpdate[i] = 0;
	}

	for (int i = 0; i < 416; i++) {
		keyState[i] = false;
		keyUpdate[i] = 0;
	}

	for (int i=0; i < 16; i++) {
		gamePadState[i] = false;
		gamePadUpdate[i] = 0;
	}

	quit = false;
	updateCounter = 0;
	mouseX = 0;
	mouseY = 0;
	mouseWheel = 0;
	screenResized = false;
}

InputHandler& InputHandler::getInstance() {
	static InputHandler instance;
	return instance;
}

void InputHandler::update() {
	SDL_Event event;

	updateCounter++;
	quit = false;
	screenResized = false;
	mouseWheel = 0;
	inputText.erase();

	SDL_GetMouseState(&mouseX, &mouseY);

	while (SDL_PollEvent(&event)) {

    if (event.type == SDL_QUIT) {
			quit = true;
		}

    else if (event.type == SDL_MOUSEBUTTONDOWN) {
			mouseState[event.button.button] = true;
			mouseUpdate[event.button.button] = updateCounter;
		}

    else if (event.type == SDL_MOUSEBUTTONUP) {
			mouseState[event.button.button] = false;
			mouseUpdate[event.button.button] = updateCounter;
		}

    else if (event.type == SDL_MOUSEWHEEL) {
			mouseWheel = -event.wheel.y;
		}

    else if (event.type == SDL_KEYDOWN && !event.key.repeat) {
        if (event.key.keysym.sym < 0x40000000) {
				  keyState[event.key.keysym.sym] = true;
          keyUpdate[event.key.keysym.sym] = updateCounter;
        } else {
				  keyState[event.key.keysym.sym - 0x3FFFFF81] = true;
				  keyUpdate[event.key.keysym.sym - 0x3FFFFF81] = updateCounter;
			  }
		}

		else if (event.type == SDL_KEYUP && !event.key.repeat) {
			if (event.key.keysym.sym < 0x40000000) {
				keyState[event.key.keysym.sym] = false;
				keyUpdate[event.key.keysym.sym] = updateCounter;
			} else {
				keyState[event.key.keysym.sym - 0x3FFFFF81] = false;
				keyUpdate[event.key.keysym.sym - 0x3FFFFF81] = updateCounter;
			}
		}

		else if (event.type == SDL_TEXTINPUT) {
			inputText += event.text.text;
		}

		else if (event.type == SDL_WINDOWEVENT) {
			if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
				Resources::WINDOW_WIDTH = event.window.data1;
				Resources::WINDOW_HEIGHT = event.window.data2;
				screenResized = true;
			}
		}

		// Analogic (LA RA)
		else if (event.type == SDL_CONTROLLERAXISMOTION) {
			if (event.caxis.which == 0) {
				//X axis motion
				if (event.caxis.axis == 0) {
					if (event.caxis.value < -joystickDeadZone) {
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = true;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = updateCounter;
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = false;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = updateCounter;
					} else if (event.caxis.value > joystickDeadZone) {
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = true;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = updateCounter;
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = false;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = updateCounter;
					} else {
						gamePadState[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = false;
						gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = updateCounter;
						gamePadState[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = false;
						gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = updateCounter;
					}
				}
				//Y axis motion
				if (event.caxis.axis == 1) {
					if (event.caxis.value < -joystickDeadZone) {
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_UP] = true;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_UP] = updateCounter;
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = false;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = updateCounter;
					} else if (event.caxis.value > joystickDeadZone) {
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = true;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = updateCounter;
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_UP] = false;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_UP] = updateCounter;
					} else {
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = false;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = updateCounter;
            gamePadState[SDL_CONTROLLER_BUTTON_DPAD_UP] = false;
            gamePadUpdate[SDL_CONTROLLER_BUTTON_DPAD_UP] = updateCounter;
					}
				}
			}
		}

		// Button Events (LB RB ARROWS YXBA)
		else if (event.type == SDL_JOYBUTTONDOWN) {
				gamePadState[event.cbutton.button] = true;
				gamePadUpdate[event.cbutton.button] = updateCounter;
		}

		else if (event.type == SDL_JOYBUTTONUP) {
			gamePadState[event.cbutton.button] = false;
			gamePadUpdate[event.cbutton.button] = updateCounter;
		}

		if (event.type == SDL_JOYBALLMOTION) {
			std::cout << "SDL_JOYBALLMOTION" << std::endl;
		}

		if (event.type == SDL_JOYHATMOTION) {
			std::cout << "SDL_JOYHATMOTION" << std::endl;
		}
	}
}

bool InputHandler::keyPress(int key) {
	int i = key;
	if (key >= 0x40000000) {
		i -= 0x3FFFFF81;
	}

	if (keyState[i] && keyUpdate[i] == updateCounter) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::keyRelease(int key) {
	int i = key;
	if (key >= 0x40000000) {
		i -= 0x3FFFFF81;
	}

	if (!keyState[i] && keyUpdate[i] == updateCounter) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::isKeyDown(int key) {
	int i = key;
	if (key >= 0x40000000) {
		i -= 0x3FFFFF81;
	}

	if (keyState[i]) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::mousePress(int button) {
	if (mouseState[button] && mouseUpdate[button] == updateCounter) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::mouseRelease(int button) {
	if (!mouseState[button] && mouseUpdate[button] == updateCounter) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::isMouseDown(int button) {
	if (mouseState[button]) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::gamePadPress(int button) {
	if (gamePadState[button] && gamePadUpdate[button] == updateCounter) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::gamePadRelease(int button) {
	if (!gamePadState[button] && gamePadUpdate[button] == updateCounter) {
		return true;
	} else {
		return false;
	}
}

bool InputHandler::isGamePadDown(int button) {
	if (gamePadState[button]) {
		return true;
	} else {
		return false;
	}
}

void InputHandler::setJoystickDeadZone(int value) {
  joystickDeadZone = value;
}

void InputHandler::setControllerInstanceId(int id) {
  controllerInstanceId_ = id;
}

bool InputHandler::isCrossPlatformInputPress(int button) {
  return keyPress(button) || gamePadPress(button);
}

bool InputHandler::isCrossPlatformInputRelease(int button) {
  return keyRelease(button) || gamePadRelease(button);
}

bool InputHandler::isCrossPlatformDown(int button) {
  return isKeyDown(button) || isGamePadDown(button);
}

int InputHandler::getMouseX() {
	return mouseX/* / Resources::GLOBAL_SCALE_X*/;
}

int InputHandler::getMouseY() {
	return mouseY/* / Resources::GLOBAL_SCALE_Y*/;
}

Vec InputHandler::getMouse() {
	return Vec(mouseX/* / Resources::GLOBAL_SCALE_X*/, mouseY/* / Resources::GLOBAL_SCALE_Y*/);
}

bool InputHandler::quitRequested() {
	return quit;
}

bool InputHandler::getScreenResized() {
	return screenResized;
}

bool InputHandler::textInput() {
	return !inputText.empty();
}

std::string InputHandler::getText() {
	return inputText;
}

bool InputHandler::mouseWheelScroll() {
	return mouseWheel != 0;
}

int InputHandler::mouseWheelAmount() {
	return mouseWheel;
}
