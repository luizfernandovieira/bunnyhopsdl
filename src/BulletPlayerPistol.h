#ifndef BULLET_PLAYER_PISTOL_H
#define BULLET_PLAYER_PISTOL_H

#include "Bullet.h"
#include "Sprite.h"

class BulletPlayerPistol : public Bullet {

public:
	BulletPlayerPistol(int x = 0, int y = 0);
	~BulletPlayerPistol();
	void update(float dt);
	void render();
  void notifyCollision(GameObject & object);
  bool is(const std::string& type);
  bool isDead();

private:
  Sprite sp_;
  int moveSpeed_;

};

#endif
