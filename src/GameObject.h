#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>

#include "Rect.h"

class GameObject {

public:
  GameObject() : rotation_(0) {}
	virtual ~GameObject() {};
	virtual void update(float dt) = 0;
	virtual void render() = 0;
	virtual void notifyCollision(GameObject& object) = 0;
	virtual bool is(const std::string& type) = 0;
  virtual bool isDead() = 0;
	Rect box_;
	float rotation_ = 0;
	bool isDead_ = false;

};

#endif
