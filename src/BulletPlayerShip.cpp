#include "BulletPlayerShip.h"
#include "Camera.h"

#include <iostream>

BulletPlayerShip::BulletPlayerShip(int x, int y) :
  sp_("../img/bullets/bullet_player_ship.png"),
  moveSpeed_(1) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

BulletPlayerShip::~BulletPlayerShip() {

}

void BulletPlayerShip::update(float dt) {
  box_.x(box_.x() + moveSpeed_);

  if (box_.x() > Camera::rightEdge(this)) {
    isDead_ = true;
  }
}

void BulletPlayerShip::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void BulletPlayerShip::notifyCollision(GameObject & object) {

}

bool BulletPlayerShip::is(const std::string& type) {
  return false;
}

bool BulletPlayerShip::isDead() {
  return isDead_;
}
