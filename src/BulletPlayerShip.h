#ifndef BULLET_PLAYER_SHIP_H
#define BULLET_PLAYER_SHIP_H

#include "Bullet.h"
#include "Sprite.h"

class BulletPlayerShip : public Bullet {

public:
	BulletPlayerShip(int x = 0, int y = 0);
	~BulletPlayerShip();
	void update(float dt);
	void render();
  void notifyCollision(GameObject & object);
  bool is(const std::string& type);
  bool isDead();

private:
  Sprite sp_;
  int moveSpeed_;

};

#endif
