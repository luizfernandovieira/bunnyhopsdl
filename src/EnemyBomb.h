 #ifndef ENEMY_BOMB_H
 #define ENEMY_BOMB_H

 #include "IEnemy.h"
 #include "Sprite.h"

 enum class EnemyBombState {
   FLYING = 0
 };

 class EnemyBomb : public IEnemy {

 public:
 	EnemyBomb(int x = 0, int y = 0);
 	~EnemyBomb();
 	void update(float dt) override;
 	void render() override;
   void notifyCollision(GameObject& object) override;
   bool is(const std::string& type) override;
   bool isDead() override;

 protected:
   Sprite sp_;
   EnemyBombState state_;

 };

 #endif
