#include "Player.h"
#include "Camera.h"
#include "Game.h"
#include "InputHandler.h"

Player::Player(int x, int y) :
  idleSp_("../img/entity/player/player_idle.png", 1, 0),
  walkingSp_("../img/entity/player/player_idle.png", 1, 0),
  jumpingSp_("../img/entity/player/player_jumping.png", 1, 0),
  idleInfectedSp_("../img/entity/player/player_infected_idle.png", 1, 0),
  walkingInfectedSp_("../img/entity/player/player_idle.png", 1, 0),
  jumpingInfectedSp_("../img/entity/player/player_infected_jumping.png", 1, 0) {
  box_.x(x);
  box_.y(y);
  box_.w(walkingSp_.getWidth());
  box_.h(walkingSp_.getHeight());
}

Player::~Player() {

}

void Player::update(float dt) {
  walkingSp_.update(dt);
}

void Player::render() {
  idleSp_.render();
  walkingSp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void Player::notifyCollision(GameObject & object) {

}

bool Player::is(const std::string& type) {
  return false;
}

bool Player::isDead() {
  return false;
}

void Player::shoot() {

}