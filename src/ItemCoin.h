#ifndef ITEM_COIN_H
#define ITEM_COIN_H

#include "IItem.h"
#include "Sprite.h"

class ItemCoin : public IItem {

public:
    ItemCoin(int x = 0, int y = 0);
    ~ItemCoin();
    void update(float dt) override;
    void render() override;
    void notifyCollision(GameObject& object) override;
    bool is(const std::string& type) override;
    bool isDead() override;

protected:
    Sprite sp_;

};

#endif
