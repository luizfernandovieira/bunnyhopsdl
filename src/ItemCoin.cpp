#include "ItemCoin.h"
#include "Camera.h"

ItemCoin::ItemCoin(int x, int y) :
    sp_("../img/item/coin.png", 2, 0.1) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

ItemCoin::~ItemCoin() {

}

void ItemCoin::update(float dt) {

}

void ItemCoin::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void ItemCoin::notifyCollision(GameObject& object) {

}

bool ItemCoin::is(const std::string& type) {
  return false;
}

bool ItemCoin::isDead() {
  return false;
}
