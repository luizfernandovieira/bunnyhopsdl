#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include "SDL.h"

#include <iostream>
#include <vector>
#include <string>

#include "Vec.h"

#define LEFT_ARROW_KEY 		SDLK_LEFT
#define RIGHT_ARROW_KEY 	SDLK_RIGHT
#define UP_ARROW_KEY 		SDLK_UP
#define DOWN_ARROW_KEY 		SDLK_DOWN
#define ESCAPE_KEY 			SDLK_ESCAPE
#define LEFT_MOUSE_BUTTON 	SDL_BUTTON_LEFT
#define RIGHT_MOUSE_BUTTON 	SDL_BUTTON_RIGHT
#define SPACE_BAR 			SDLK_SPACE
#define BACKSPACE			SDLK_BACKSPACE
#define ENTER			    SDLK_RETURN

#define W_KEY SDLK_w
#define A_KEY SDLK_a
#define S_KEY SDLK_s
#define D_KEY SDLK_d

#define NUMKEY_1			SDLK_1
#define NUMKEY_2			SDLK_2
#define NUMKEY_3			SDLK_3
#define NUMKEY_4			SDLK_4
#define NUMKEY_5			SDLK_5
#define NUMKEY_6			SDLK_6
#define NUMKEY_7			SDLK_7
#define NUMKEY_8			SDLK_8

#define GAMEPAD_A 			    SDL_CONTROLLER_BUTTON_A
#define GAMEPAD_B 			    SDL_CONTROLLER_BUTTON_B
#define GAMEPAD_X 			    SDL_CONTROLLER_BUTTON_X
#define GAMEPAD_Y 			    SDL_CONTROLLER_BUTTON_Y
#define GAMEPAD_LB 			    SDL_CONTROLLER_BUTTON_LEFTSHOULDER
#define GAMEPAD_RB 			    SDL_CONTROLLER_BUTTON_RIGHTSHOULDER
#define GAMEPAD_ARROW_UP 	  SDL_CONTROLLER_BUTTON_DPAD_UP
#define GAMEPAD_ARROW_DOWN 	SDL_CONTROLLER_BUTTON_DPAD_DOWN
#define GAMEPAD_ARROW_LEFT 	SDL_CONTROLLER_BUTTON_DPAD_LEFT
#define GAMEPAD_ARROW_RIGHT SDL_CONTROLLER_BUTTON_DPAD_RIGHT

#define LT 0x20
#define RT 0x21
#define SELECT SDL_CONTROLLER_BUTTON_BACK
#define START SDL_CONTROLLER_BUTTON_START
#define L3 SDL_CONTROLLER_BUTTON_LEFTSTICK
#define R3 SDL_CONTROLLER_BUTTON_RIGHTSTICK

class InputHandler {

public:
	static InputHandler& getInstance();
	void update();
	bool keyPress(int key);
	bool keyRelease(int key);
	bool isKeyDown(int key);
	bool textInput();
	std::string getText();
	bool mousePress(int button);
	bool mouseRelease(int button);
	bool isMouseDown(int button);
	bool mouseWheelScroll();
	int mouseWheelAmount();
	int getMouseX();
	int getMouseY();
	Vec getMouse();
	bool gamePadPress(int button);
	bool gamePadRelease(int button);
	bool isGamePadDown(int button);
  void setControllerInstanceId(int id);
  void setJoystickDeadZone(int value);
  bool isCrossPlatformInputPress(int button);
  bool isCrossPlatformInputRelease(int button);
  bool isCrossPlatformDown(int button);
	bool getScreenResized();
	bool quitRequested();

private:
	InputHandler();
	~InputHandler(){}
	bool mouseState[6];
	int mouseUpdate[6];
	int mouseWheel;
	int mouseX;
	int mouseY;
	bool keyState[416];
	int keyUpdate[416];
	bool gamePadState[15];
	int gamePadUpdate[15];
	bool quit;
	int updateCounter;
	int joystickDeadZone = 32000; //10000;
	bool screenResized;
	std::string inputText;
  int controllerInstanceId_;

};

#endif
