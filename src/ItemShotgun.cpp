#include "ItemShotgun.h"
#include "Camera.h"

ItemShotgun::ItemShotgun(int x, int y) :
    sp_("../img/item/shotgun.png", 1, 0.1) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

ItemShotgun::~ItemShotgun() {

}

void ItemShotgun::update(float dt) {

}

void ItemShotgun::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void ItemShotgun::notifyCollision(GameObject& object) {

}

bool ItemShotgun::is(const std::string& type) {
  return false;
}

bool ItemShotgun::isDead() {
  return false;
}
