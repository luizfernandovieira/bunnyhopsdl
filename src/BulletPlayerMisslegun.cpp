#include "BulletPlayerMisslegun.h"
#include "Camera.h"

BulletPlayerMisslegun::BulletPlayerMisslegun(int x, int y) :
  sp_("../img/bullets/bullet_player_misslegun.png"),
  moveSpeed_(50) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

BulletPlayerMisslegun::~BulletPlayerMisslegun() {

}

void BulletPlayerMisslegun::update(float dt) {

}

void BulletPlayerMisslegun::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void BulletPlayerMisslegun::notifyCollision(GameObject & object) {

}

bool BulletPlayerMisslegun::is(const std::string& type) {
  return false;
}

bool BulletPlayerMisslegun::isDead() {
  return false;
}
