#ifndef BULLET_PLAYER_MISSLEGUN_H
#define BULLET_PLAYER_MISSLEGUN_H

#include "Bullet.h"
#include "Sprite.h"

class BulletPlayerMisslegun : public Bullet {

public:
	BulletPlayerMisslegun(int x = 0, int y = 0);
	~BulletPlayerMisslegun();
	void update(float dt);
	void render();
  void notifyCollision(GameObject & object);
  bool is(const std::string& type);
  bool isDead();

private:
  Sprite sp_;
  int moveSpeed_;

};

#endif
