#ifndef ITEM_MACHINEGUN_H
#define ITEM_MACHINEGUN_H

#include "IItem.h"
#include "Sprite.h"

class ItemMachinegun : public IItem {

public:
    ItemMachinegun(int x = 0, int y = 0);
    ~ItemMachinegun();
    void update(float dt) override;
    void render() override;
    void notifyCollision(GameObject& object) override;
    bool is(const std::string& type) override;
    bool isDead() override;

protected:
    Sprite sp_;

};

#endif
