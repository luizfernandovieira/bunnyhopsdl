#include "Game.h"
#include "Resources.h"
#include "SplashState.h"

int main(int argc, char* argv[]) {
  Game game("Bunny Hop", Resources::WINDOW_WIDTH, Resources::WINDOW_HEIGHT, Resources::FULLSCREEN);
  game.changeResolution(Resources::WINDOW_WIDTH * Resources::SCALE, Resources::WINDOW_HEIGHT * Resources::SCALE);
  game.push(new SplashState());
  game.run();
  return 0;
}
