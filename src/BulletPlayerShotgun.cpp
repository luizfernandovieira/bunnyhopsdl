#include "BulletPlayerShotgun.h"
#include "Camera.h"

BulletPlayerShotgun::BulletPlayerShotgun(int x, int y) :
  sp_("../img/bullets/bullet_player_shotgun.png"),
  moveSpeed_(50) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

BulletPlayerShotgun::~BulletPlayerShotgun() {

}

void BulletPlayerShotgun::update(float dt) {

}

void BulletPlayerShotgun::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void BulletPlayerShotgun::notifyCollision(GameObject & object) {

}

bool BulletPlayerShotgun::is(const std::string& type) {
  return false;
}

bool BulletPlayerShotgun::isDead() {
  return false;
}
