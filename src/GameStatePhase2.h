#ifndef GAME_STATE_H
#define GAME_STATE_H

#include <vector>
#include <memory>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <stdexcept>

#include "State.h"
#include "PlayerShip.h"
#include "Tmx.h.in"
#include "Text.h"

class GameStatePhase2 : public State {

public:
	GameStatePhase2();
	~GameStatePhase2();
	void create();
	void update(float dt);
	void render();
	void pause();
	void resume();

private:
  Sprite bg_;
  PlayerShip* player_;
  Tmx::Map* map_;
  Sprite hud_;
  Text* stageHud_;
  Text* lifeHud_;
  Text* bulletsHud_;
  Text* weaponHud_;
  Text* stageHudValue_;
  Text* lifeHudValue_;
  Text* bulletsHudValue_;
  Text* weaponHudValue_;
};

#endif
