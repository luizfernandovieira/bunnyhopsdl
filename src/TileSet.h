#pragma once

#include <string>

#include "Sprite.h"

class TileSet {
  public:
    TileSet(int tileWidth, int tileHeight, std::string file);
    void render(unsigned index, float x, float y);
    int getTileWidth();
    int getTileHeight();
  private:
    Sprite tileSet_;
    int rows_;
    int columns_;
    int tileWidth_;
    int tileHeight_;
};
