#ifndef PRINCESS_H
#define PRINCESS_H

#include "GameObject.h"
#include "Sprite.h"

class Princess : public GameObject {

public:
    Princess(int x = 0, int y = 0);
    ~Princess();
    void update(float dt);
    void render();
    void notifyCollision(GameObject &object);
    bool is(const std::string &type);
    bool isDead();
private:
    Sprite sp_;
};

#endif
