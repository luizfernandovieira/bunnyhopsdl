#ifndef BULLET_H
#define BULLET_H

#include "GameObject.h"

class Bullet : public GameObject {

public:
  Bullet() {}
	virtual ~Bullet() {};
	virtual void update(float delta) = 0;
	virtual void render() = 0;
	virtual void notifyCollision(GameObject & object) = 0;
	virtual bool is(const std::string& type) = 0;
  virtual bool isDead() = 0;

};

#endif
