#include "Rect.h"

Rect::Rect() {
	x_ = 0.0;
	y_ = 0.0;
	w_ = 0.0;
	h_ = 0.0;
}

Rect::Rect(float x, float y, float w, float h) {
	x_ = x;
	y_ = y;
	w_ = w;
	h_ = h;
}

Vec Rect::getCenter() const {
	return Vec(x_+(w_/2), y_+(h_/2));
}

Vec Rect::getPivot() const {
	return Vec(x_, y_);
}

Vec Rect::getBase() const {
	return Vec(x_+(w_/2), y_ + h_);
}

Vec Rect::getBottomCenter() const {
  return Vec(x_+(w_/2), y_ + h_);
}

Vec Rect::getTopCenter() const {
  return Vec(x_+(w_/2), y_);
}

Vec Rect::getLeftCenter() const {
  return Vec(x_, y_ +(h_/2));
}

Vec Rect::getRightCenter() const {
  return Vec(x_+ w_, y_ +(h_/2));
}

Rect Rect::operator+ (const Vec& vec) {
	Rect rect;
	rect.x(x_ + vec.x());
	rect.y(y_ + vec.y());
	rect.w(w_);
	rect.h(h_);
	return rect;
}

Rect& Rect::operator+= (const Vec& vec) {
	x_ += vec.x();
	y_ += vec.y();
	return (*this);
}

Rect Rect::operator* (const Rect& r) {
	Rect rect;
	rect.x(x_ * r.x());
	rect.y(y_ * r.y());
	rect.w(w_ * r.w());
	rect.h(h_ * r.h());
	return rect;
}

bool Rect::operator== (const Rect& rect) {
	return (x_ == rect.x() && y_ == rect.y() && w_ == rect.w() && h_ == rect.h());
}

bool Rect::operator!= (const Rect& rect) {
	return (!(*this == rect));
}

bool Rect::isInside(Vec vec) {
	if (vec.x() >= x_ &&
		  vec.x() <= x_ + w_ &&
		  vec.y() >= y_ &&
		  vec.y() <= y_ + h_) {
    return true;
  } else {
		return false;
  }
}

float Distance(Rect r1, Rect r2) {
	return Distance(r1.getCenter(), r2.getCenter());
}
