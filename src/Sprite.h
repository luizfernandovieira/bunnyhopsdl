#pragma once

#include <string>
#include <memory>

#include "SDL.h"

#define PI 3.14159265359

class Sprite {
public:
    Sprite();
    Sprite(const std::string& file, int frameCount = 1, float frameTime = 1.0f);
    ~Sprite();
    void open(const std::string& file);
    void render(int x = 0, int y = 0, float angle = 0) const;
    void setClip(int x, int y, int w, int h);
    int getWidth() const;
    int getHeight() const;
    bool isOpen() const;
    void setScaleX (float scale);
    void setScaleY (float scale);
    void update (float dt);
    void setFrame (int frame);
    void setFrameCount (int frameCount);
    void setFrameTime (float frameTime);
    SDL_Texture* getTexture();

private:
    std::shared_ptr<SDL_Texture> texture;
    SDL_Rect clipRect;
    int width;
    int height;
    float scaleX;
    float scaleY;
    int frameCount;
    int currentFrame;
    float timeElapsed;
    float frameTime;

};
