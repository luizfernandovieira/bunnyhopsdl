#include "ItemMachinegun.h"
#include "Camera.h"

ItemMachinegun::ItemMachinegun(int x, int y) :
    sp_("../img/item/machinegun.png", 1, 0.1) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

ItemMachinegun::~ItemMachinegun() {

}

void ItemMachinegun::update(float dt) {

}

void ItemMachinegun::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void ItemMachinegun::notifyCollision(GameObject& object) {

}

bool ItemMachinegun::is(const std::string& type) {
  return false;
}

bool ItemMachinegun::isDead() {
  return false;
}
