#include "Camera.h"
#include "Resources.h"

Vec Camera::pos_;
Vec Camera::speed_;
GameObject* Camera::focus_;
bool Camera::spaceShooter_ = false;
bool Camera::moving_ = false;
float Camera::moveSpeed_ = 0.2;

void Camera::follow(GameObject* newFocus) {
	focus_ = newFocus;
	pos_ = Vec(0,0);
}

void Camera::unfollow() {
	focus_ = NULL;
}

void Camera::startPlaformer() {
	spaceShooter_ = false;
}

void Camera::startSpaceShooter() {
	spaceShooter_ = true;
}

void Camera::startMoving() {
	moving_ = true;
}

void Camera::stopMoving() {
	moving_ = false;
}

void Camera::update(float dt) {
	if (focus_ != NULL) {
		// float focusX = focus_->box.getCenter().x() - 512;
		// float focusY = focus_->box.getCenter().y() - 288;

		// if (focusX > pos.x + CAMERA_WINDOW_WIDTH_HALF && pos.x < maxWidth - 1034) {
    //   pos.x = focusX - CAMERA_WINDOW_WIDTH_HALF;
    // } else if (focusX < pos.x - CAMERA_WINDOW_WIDTH_HALF && pos.x > 10) {
    //   pos.x = focusX + CAMERA_WINDOW_WIDTH_HALF;
    // }
    //
		// if (focusY > pos.y + CAMERA_WINDOW_HEIGHT_HALF && pos.y < maxHeight - 586) {
		// 	pos.y = focusY - CAMERA_WINDOW_HEIGHT_HALF;
    // } else if (focusY < pos.y - CAMERA_WINDOW_HEIGHT_HALF && pos.y > 10) {
		// 	pos.y = focusY + CAMERA_WINDOW_HEIGHT_HALF;
    // }

	} else {
		if (!spaceShooter_) { // PLATFORMER

		} else { // SPACE SHOOTER
			pos_.x(pos_.x() + moveSpeed_);
		}

		// if (InputManager::GetInstance().IsKeyDown('a')) {
		// 	speed = Point(-CAMERA_SPEED * dt, 0);
		// 	pos = pos + speed;
		// }
    //
		// if (InputManager::GetInstance().IsKeyDown('d')) {
		// 	speed = Point(CAMERA_SPEED * dt, 0);
		// 	pos = pos + speed;
		// }
    //
		// if (InputManager::GetInstance().IsKeyDown('w')) {
		// 	speed = Point(0, -CAMERA_SPEED * dt);
		// 	pos = pos + speed;
		// }
    //
		// if (InputManager::GetInstance().IsKeyDown('s')) {
		// 	speed = Point(0, CAMERA_SPEED * dt);
		// 	pos = pos + speed;
		// }

	}
}

float Camera::rightEdge(GameObject* go) {
  return pos_.x() + Resources::WINDOW_WIDTH + go->box_.w();
}

float Camera::leftEdge(GameObject* go) {
  return pos_.x() - go->box_.w();
}
