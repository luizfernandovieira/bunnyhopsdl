#ifndef PLAYER_SHIP_H
#define PLAYER_SHIP_H

#include "GameObject.h"
#include "Sprite.h"
#include "Timer.h"

enum PlayerShipState {
  FLYING,
  DYING
};

class PlayerShip : public GameObject {

public:
	PlayerShip(int x = 0, int y = 0);
	~PlayerShip();
	void update(float dt);
	void render();
  void notifyCollision(GameObject& object);
  bool is(const std::string& type);
  bool isDead();
  void shoot();

private:
  Sprite flyingSp_;
  Sprite dyingSp_;
  PlayerShipState state_;
  int moveUp_;
  int moveDown_;
  int moveRight_;
  int moveLeft_;
  int moveHorizontal_;
  int moveVertical_;
  int moveSpeed_;
  int gravity_;
  bool facingRight_;
  int life_;
  bool canShoot_;
  float shootCooldown_;
  Timer shootTimer_;
  bool takingDmg_;
  bool canTakeDmg_;
  float takeDmgCooldown_;
  Timer takeDmgTimer_;
  bool flicking_;
  int flickCounter_;

};

#endif
