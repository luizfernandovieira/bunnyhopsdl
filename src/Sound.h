#ifndef SOUND_H
#define SOUND_H

#include "SDL_mixer.h"

#include <iostream>
#include <string>
#include <unordered_map>
#include <memory>

class Sound {

public:
  Sound();
  Sound(const std::string& file);
  void play (int times = 0);
  void stop ();
  void open (const std::string& file);
  bool isOpen ();

private:
  std::shared_ptr<Mix_Chunk> chunk;
  int channel;

};

#endif
