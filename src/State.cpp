#include "State.h"

#include <iostream>

void State::addObject(GameObject * object){
	objects.emplace_back(object);
}

void State::updateObjects(float dt) {
  std::cout << objects.size() << std::endl;
  for(unsigned it = 0; it < objects.size(); ++it){
    objects[it]->update(dt);
    if (objects[it]->isDead()) {
      objects.erase(objects.begin() + it);
      --it;
    }
  }
}

void State::renderObjects() {
  for(auto & go : objects){
    go->render();
  }
}
