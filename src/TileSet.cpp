#include <iostream>

#include "TileSet.h"

TileSet::TileSet(int tileWidth, int tileHeight, std::string file) :
  tileWidth_(tileWidth),
  tileHeight_(tileHeight),
  tileSet_(file) {
  if (!tileSet_.isOpen()) {
    std::clog << "TileSet::Open()\t\t Sprite n�o encontrado." << SDL_GetError() << std::endl;
    rows_ = 0;
    columns_ = 0;
  } else {
    rows_ = tileSet_.getHeight() / tileHeight_;
    columns_ = tileSet_.getWidth() / tileWidth;
  }
}

void TileSet::render(unsigned index, float x, float y) {
  unsigned size = (unsigned) (rows_ * columns_);
  if (index < size) {
    tileSet_.setClip(index % columns_ * tileWidth_, index / columns_ * tileHeight_, tileWidth_, tileHeight_);
    tileSet_.render((int) x, (int) y);
  }
}

int TileSet::getTileWidth() {
  return tileWidth_;
}

int TileSet::getTileHeight() {
  return tileHeight_;
}