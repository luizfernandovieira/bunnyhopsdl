#include <string>
#include <sstream>

#include "Resources.h"
#include "Game.h"

int Resources::WINDOW_WIDTH = 160;
int Resources::WINDOW_HEIGHT = 144;
int Resources::WINDOW_BORDER = 1;
int Resources::SCALE = 4;
int Resources::FULLSCREEN = 0;
int Resources::VSYNC = 0;
int Resources::SOUND_VOLUME = 5;
int Resources::MUSIC_VOLUME = 0;

std::unordered_map<std::string, std::shared_ptr<SDL_Texture>> Resources::imageTable;
std::unordered_map<std::string, std::shared_ptr<Mix_Music>> Resources::musicTable;
std::unordered_map<std::string, std::shared_ptr<Mix_Chunk>> Resources::soundTable;
std::unordered_map<std::string, std::shared_ptr<TTF_Font>> Resources::fontTable;

std::shared_ptr<SDL_Texture> Resources::getImage(std::string file) {
	auto it = imageTable.find(file);

	if (it == imageTable.end()) {
		SDL_Texture* texture = IMG_LoadTexture(Game::getInstance().getRenderer(), file.c_str());

		if (texture == NULL) {
			std::cout << "Error loading texture " << file << ": " << IMG_GetError() << std::endl;
		}

		auto deleteTexture = [] (SDL_Texture* texture) {
			SDL_DestroyTexture(texture);
		};

		std::shared_ptr<SDL_Texture> ptr (texture, deleteTexture);
		imageTable.emplace(file, ptr);
		return ptr;
	} else {
		std::shared_ptr<SDL_Texture> texture = it->second;
		return texture;
	}
}

void Resources::clearImages() {
	for (auto it = imageTable.cbegin(); it != imageTable.cend(); ) {
		if (it->second.unique()) {
			imageTable.erase(it++);
    } else {
			++it;
    }
	}
}

std::shared_ptr<Mix_Music> Resources::getMusic(std::string file) {
  if(musicTable.find(file) == musicTable.end()) {
		Mix_Music* mx = Mix_LoadMUS(file.c_str());

		if(mx == nullptr){
			printf("%s: %s\n", Mix_GetError(), file.c_str());
			exit(-1);
		}

		std::shared_ptr<Mix_Music> music(mx, [](Mix_Music* msc) {
      Mix_FreeMusic(msc);
    });

		musicTable.emplace(file, music);
	}

  return musicTable[file];
}

void	Resources::clearMusics() {
  musicTable.clear();
}

std::shared_ptr<Mix_Chunk> Resources::getSound(std::string file) {
  if(soundTable.find(file) == soundTable.end()) {
  	Mix_Chunk* ck = Mix_LoadWAV(file.c_str());

  	if(ck == nullptr){
  		printf("%s: %s\n", Mix_GetError(), file.c_str());
  		exit(-1);
  	}

  	std::shared_ptr<Mix_Chunk> sound(ck, [](Mix_Chunk* chk) {
      Mix_FreeChunk(chk);
    });

    soundTable.emplace(file, sound);
  }

	return soundTable[file];
}

void	Resources::clearSounds() {
	// soundTable.clear();
  for(auto sound : soundTable){
    if(sound.second.unique()){
      soundTable.erase(sound.first);
    }
  }
}

std::shared_ptr<TTF_Font> Resources::getFont(std::string file, int fontSize) {
	std::ostringstream ss;
	ss << fontSize;
	auto it = fontTable.find(file + ss.str());

	if (it == fontTable.end()) {
		TTF_Font* font = TTF_OpenFont(file.c_str(), fontSize);

    if (font == NULL) {
			std::cout << "Error loading font " << file << ": " << TTF_GetError() << std::endl;
		}

		auto deleteFont = [] (TTF_Font* font) {
			TTF_CloseFont(font);
		};

		std::shared_ptr<TTF_Font> ptr (font, deleteFont);
		fontTable.emplace(file + ss.str(), ptr);
		return ptr;
	} else {
		std::shared_ptr<TTF_Font> font = it->second;
		return font;
	}
}

void Resources::clearFonts() {
	for (auto it = fontTable.cbegin() ; it != fontTable.cend(); ) {
		if (it->second.unique()) {
			fontTable.erase(it++);
    } else {
			++it;
    }
	}
}
