#include "Animation.h"

#include <iostream>

Animation::Animation(float x, float y, float rotation,
                     const std::string& sprite,
                     int frameCount, float frameTime,
                     bool ends) :
  sp_(sprite, frameCount, frameTime),
  ends_(ends),
  timeLimit_(frameTime * frameCount) {

  box_.w((float) sp_.getWidth());
  box_.h((float) sp_.getHeight());
  box_.x(x - box_.w()/2);
  box_.y(y - box_.h()/2);
  rotation_ = rotation;
}

void Animation::update(float dt) {
  endTimer_.update(dt);
  sp_.update(dt);
}

void Animation::render() {
  sp_.render((int)(box_.x() - Camera::pos_.x()), (int)(box_.y() - Camera::pos_.y()), rotation_);
}

void Animation::notifyCollision(GameObject& other) {

}

bool Animation::is(const std::string& type) {
  return (type == "Animation");
}

bool Animation::isDead() {
  return (ends_ && (endTimer_.get() >= timeLimit_));
}
