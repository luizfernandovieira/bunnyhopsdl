#include "BulletPlayerMachinegun.h"
#include "Camera.h"

BulletPlayerMachinegun::BulletPlayerMachinegun(int x, int y) :
  sp_("../img/bullets/bullet_player_machinegun.png"),
  moveSpeed_(50) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

BulletPlayerMachinegun::~BulletPlayerMachinegun() {

}

void BulletPlayerMachinegun::update(float dt) {

}

void BulletPlayerMachinegun::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void BulletPlayerMachinegun::notifyCollision(GameObject & object) {

}

bool BulletPlayerMachinegun::is(const std::string& type) {
  return false;
}

bool BulletPlayerMachinegun::isDead() {
  return false;
}
