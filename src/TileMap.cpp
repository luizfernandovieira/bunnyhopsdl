#include "TileMap.h"

#include <iostream>
#include <fstream>
#include <cstdio>

TileMap::TileMap(const std::string& file, TileSet* tileSet) {
  setTileSet(tileSet);
  load(file);
}

void TileMap::load(const std::string& mapPath) {
    int data;
    FILE* fp = fopen (mapPath.c_str(), "r");

    if (fp == NULL) {
        std::clog << "TileMap::Load\t\t Arquivo de mapa n�o encontrado." << std::endl;
        return;
    }

    fscanf(fp, "%d%*c%d%*c%d%*c", &mapWidth_, &mapHeight_, &mapDepth_);

    tileMatrix_.resize(mapWidth_ * mapHeight_ * mapDepth_);

    for (int& i : tileMatrix_) {
        fscanf(fp, "%d%*c", &data);
        i = data - 1;
    }

    fclose(fp);
}

void TileMap::setTileSet(TileSet* tileSet) {
  this->tileSet_ = tileSet;
}

int& TileMap::at(int x, int y, int z) {
  return tileMatrix_[x + mapWidth_ * y + mapWidth_ * mapHeight_ * z];
}

void TileMap::render(int cameraX, int cameraY) {
  if (tileSet_ == nullptr) {
    return;
  }

  for (int k = 0; k < mapDepth_; ++k) {
    renderLayer(k, cameraX, cameraY);
  }
}

void TileMap::renderLayer(int layer, int cameraX, int cameraY) {

  if (layer >= mapDepth_)
    return;

  if (tileSet_ == nullptr)
    return;

  for (int j = 0; j < mapHeight_; ++j) {
    for (int i = 0; i < mapWidth_; ++i) {
      tileSet_->render(at(i, j, layer),
                      (float) i * tileSet_->getTileWidth()  - cameraX,
                      (float) j * tileSet_->getTileHeight() - cameraY);
    }
  }

}

int TileMap::getWidth() const {
  return mapWidth_;
}

int TileMap::getHeight() const {
  return mapHeight_;
}

int TileMap::getDepth() const {
  return mapDepth_;
}