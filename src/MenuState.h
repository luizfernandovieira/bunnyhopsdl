#ifndef MENU_STATE_H
#define MENU_STATE_H

#include <map>
#include <vector>

#include "State.h"
#include "Sprite.h"
#include "Music.h"
#include "Sound.h"
#include "Text.h"

class MenuState : public State {

public:
	MenuState();
  ~MenuState();
	void update(float dt);
	void render();
	void pause();
	void resume();

private:
  Sprite bg_;
  std::vector<Text*> options_;
  int currentSelectedOption_;

};

#endif
