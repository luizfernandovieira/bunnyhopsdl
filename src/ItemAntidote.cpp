#include "ItemAntidote.h"
#include "Camera.h"

ItemAntidote::ItemAntidote(int x, int y) :
    sp_("../img/item/antidote.png", 1, 0.1) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

ItemAntidote::~ItemAntidote() {

}

void ItemAntidote::update(float dt) {

}

void ItemAntidote::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void ItemAntidote::notifyCollision(GameObject& object) {

}

bool ItemAntidote::is(const std::string& type) {
  return false;
}

bool ItemAntidote::isDead() {
  return false;
}
