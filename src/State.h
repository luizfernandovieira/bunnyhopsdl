#ifndef State_H
#define State_H

#include <vector>
#include <memory>

#include "GameObject.h"

class State {

	public:
		State() : quit_(false), pop_(false) {}
		virtual ~State() {}
		virtual void update(float dt) = 0;
		virtual void render() = 0;
		virtual void pause() = 0;
		virtual void resume() = 0;
		bool quitRequested() { return quit_; }
		bool popRequested() { return pop_; }
		virtual void addObject(GameObject* object);

	protected:
		virtual void updateObjects(float dt);
		virtual void renderObjects();
		std::vector<std::unique_ptr<GameObject>> objects;
		bool quit_;
		bool pop_;

};

#endif
