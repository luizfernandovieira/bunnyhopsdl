#include "Music.h"
#include "Resources.h"

Music::Music() : music(nullptr) {

}

Music::Music(const std::string& file) {
    open(file);
}

void Music::play (int times) {
    if (isOpen()) {
        Mix_PlayMusic (music.get(), times == -1 ? times : times - 1);
    }
}

void Music::stop () {
    Mix_FadeOutMusic(1000);
}

void Music::open (const std::string& file) {
    music = Resources::getMusic(file);
}

bool Music::isOpen () {
    return (music != nullptr);
}
