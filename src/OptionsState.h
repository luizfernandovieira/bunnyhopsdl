#ifndef OPTIONS_STATE_H
#define OPTIONS_STATE_H

#include <map>
#include <vector>

#include "State.h"
#include "Sprite.h"
#include "Sound.h"
#include "Text.h"

class OptionsState : public State {

public:
	OptionsState();
  ~OptionsState();
	void update(float dt);
	void render();
	void pause();
	void resume();

private:
  Sprite bg_;
  std::vector<Text*> options_;
  std::vector<Text*> optionsValues_;
  int currentSelectedOption_;

};

#endif
