#ifndef VEC_H
#define VEC_H

#include <cmath>

#define PI 3.14159265359

class Vec {
public:
	Vec();
	Vec(float x, float y);
	Vec operator+ (const Vec& v);
	Vec operator- (const Vec& v);
	Vec& operator+= (const Vec& v);
	Vec& operator-= (const Vec& v);
	Vec operator+ (const float& e) const;
	Vec operator- (const float& e) const;
	Vec operator* (const float& e) const;
  bool operator== (const Vec& v) const;
	void rotate(float angle);
	void rotate(Vec vec, float module, float angle);
	float x() const { return x_; }
	float y() const { return y_; }
	void x(float x) { x_ = x; }
	void y(float y) { y_ = y; }

private:
	float x_;
	float y_;

};

float Distance(Vec v1, Vec v2);
float LineInclination(Vec v1, Vec v2);
float Magnitude(const Vec& v);
float Dot(const Vec& a, const Vec& b);
float ProjectX(float module, float angle);
float ProjectY(float module, float angle);
Vec Normalize(const Vec& v);

#endif
