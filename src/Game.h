#ifndef GAME_H
#define GAME_H

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_gamecontroller.h"
#include "SDL_mixer.h"
#include "SDL_ttf.h"

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <stack>
#include <memory>

#include "State.h"

class Game {

public:
	explicit Game(const char* name, int width, int height, int fullscreen);
	Game(const Game&) = delete;
	Game& operator=(const Game&) = delete;
	~Game();
	static Game& getInstance();
	SDL_Renderer* getRenderer();
	float getDeltaTime();
	void run();
	void push(State* state);
	State& getCurrentState();
	void changeResolution(int width, int height);
	void setWindowBordered(bool on = true);
	void setFullscreen(bool on = false);
	void setVsync(bool on = false);

private:
	static Game* instance_;
	void initSDL();
	void initWindow(const char* name, int width, int height, int fullscreen);
	void initRenderer();
	void initControllers();
	void initAudio();
	void initTTF();
	void calculateDeltaTime();
	void updateResolution();
	SDL_Window* window_;
	SDL_Renderer* renderer_;
	SDL_GameController* controller_;
	float dt_;
	int frameStart_;
	int width_;
	int height_;
	std::stack<std::unique_ptr<State>> stateStack_;
	State* storedState_;

};

#endif
