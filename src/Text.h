#ifndef TEXT_H
#define TEXT_H

#include "SDL_ttf.h"
#include "SDL.h"

#include <string>
#include <memory>

#include "Rect.h"

namespace Colors {
	static SDL_Color black = {0, 0, 0, 255};
	static SDL_Color white = {255, 255, 255, 0};
  static SDL_Color gray = {124, 124, 124, 255};
  static SDL_Color red = {255, 0, 0, 255};
  static SDL_Color green = {0, 255, 0, 255};
  static SDL_Color blue = {0, 0, 255, 255};
  static SDL_Color brown = {123, 114, 99, 255};
  static SDL_Color beige = {230, 214, 156, 255};
}

class Text {

public:
	enum TextStyle { SOLID, SHADED, BLENDED };
	Text(std::string fontFile,
			int fontSize,
			TextStyle style,
			std::string text,
			SDL_Color color,
			int x = 0,
			int y = 0);
	~Text();
	void render(int cameraX = 0, int cameraY = 0);
	void setPos(int x, int y, bool centerX = true, bool centerY = true);
	void setText(std::string text);
	void setColor(SDL_Color color);
	void setStyle(TextStyle style);
	void setFontSize(int fontSize);
	std::string getText();
	Rect& getPos();

private:
	void remakeTexture();
	std::shared_ptr<TTF_Font> font;
	SDL_Texture* texture;
	std::string text;
	TextStyle style;
	int fontSize;
	SDL_Color color;
	Rect box;

};

#endif
