#ifndef CAMERA_H
#define CAMERA_H

#include "GameObject.h"
#include "Vec.h"

#define CAMERA_SPEED 300

class Camera {

public:
	static void follow(GameObject* focus);
	static void unfollow();
	static void startPlaformer();
	static void startSpaceShooter();
	static void startMoving();
	static void stopMoving();
	static void update(float dt);
	static float rightEdge(GameObject* go);
	static float leftEdge(GameObject* go);
	static Vec pos_;
	static Vec speed_;

private:
	static GameObject* focus_;
	static bool spaceShooter_;
	static bool moving_;
	static float moveSpeed_;

};

#endif
