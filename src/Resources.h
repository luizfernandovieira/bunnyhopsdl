#ifndef RESOURCES_H
#define RESOURCES_H

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"
#include "SDL_ttf.h"

#include <unordered_map>
#include <string>
#include <memory>

class Resources {

public:
	static int WINDOW_WIDTH;
	static int WINDOW_HEIGHT;
  static int WINDOW_BORDER;
  static int SCALE;
  static int FULLSCREEN;
  static int VSYNC;
  static int SOUND_VOLUME;
  static int MUSIC_VOLUME;

	static std::shared_ptr<SDL_Texture> getImage(std::string file);
	static void clearImages();
  static std::shared_ptr<Mix_Music> getMusic(std::string file);
  static void clearMusics();
	static std::shared_ptr<Mix_Chunk>	getSound(std::string file);
	static void clearSounds();
	static std::shared_ptr<TTF_Font> getFont(std::string file, int fontSize);
	static void clearFonts();

private:
	static std::unordered_map<std::string, std::shared_ptr<SDL_Texture>> imageTable;
  static std::unordered_map<std::string, std::shared_ptr<Mix_Music>> musicTable;
	static std::unordered_map<std::string, std::shared_ptr<Mix_Chunk>> soundTable;
	static std::unordered_map<std::string, std::shared_ptr<TTF_Font>> fontTable;

};

#endif
