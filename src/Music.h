#ifndef MUSIC_H
#define MUSIC_H

#include "SDL_mixer.h"

#include <string>
#include <unordered_map>
#include <memory>

class Music {

public:
  Music();
  Music(const std::string& file);
  void play(int times = -1);
  void stop();
  void open(const std::string& file);
  bool isOpen ();

private:
    std::shared_ptr<Mix_Music> music;

};

#endif
