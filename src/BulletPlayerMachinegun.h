#ifndef BULLET_PLAYER_MACHINEGUN_H
#define BULLET_PLAYER_MACHINEGUN_H

#include "Bullet.h"
#include "Sprite.h"

class BulletPlayerMachinegun : public Bullet {

public:
	BulletPlayerMachinegun(int x = 0, int y = 0);
	~BulletPlayerMachinegun();
	void update(float dt);
	void render();
  void notifyCollision(GameObject & object);
  bool is(const std::string& type);
  bool isDead();

private:
  Sprite sp_;
  int moveSpeed_;

};

#endif
