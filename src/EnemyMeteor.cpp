#include "EnemyMeteor.h"
#include "Camera.h"
#include "Game.h"

EnemyMeteor::EnemyMeteor(int x, int y, EnemyMeteorSize size) {

  switch (size) {
    case EnemyMeteorSize::SMALL:
      sp_ = Sprite("../img/entity/enemies/meteor_sm.png", 1, 0.1);
      break;
    case EnemyMeteorSize::MEDIUM:
      sp_ = Sprite("../img/entity/enemies/meteor_md.png", 1, 0.1);
      break;
    case EnemyMeteorSize::LARGE:
      sp_ = Sprite("../img/entity/enemies/meteor_lg.png", 1, 0.1);
      break;
    default:
      sp_ = Sprite("../img/entity/enemies/meteor_sm.png", 1, 0.1);
  }

  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

EnemyMeteor::~EnemyMeteor() {

}

void EnemyMeteor::update(float dt) {

}

void EnemyMeteor::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void EnemyMeteor::notifyCollision(GameObject& object) {

}

bool EnemyMeteor::is(const std::string& type) {
  return false;
}

bool EnemyMeteor::isDead() {
  return false;
}
