#ifndef RECT_H
#define RECT_H

#include "Vec.h"

class Rect {

public:
	Rect();
	Rect(float x, float y, float w, float h);
	Vec getCenter() const;
	Vec getPivot() const;
	Vec getBase() const;
  Vec getBottomCenter() const;
  Vec getTopCenter() const;
  Vec getLeftCenter() const;
  Vec getRightCenter() const;
	Rect operator+ (const Vec&);
	Rect& operator+= (const Vec&);
	Rect operator* (const Rect&);
	bool operator== (const Rect&);
	bool operator!= (const Rect&);
	bool isInside(Vec);
	float x() const { return x_; }
	float y() const { return y_; }
	float w() const { return w_; }
	float h() const { return h_; }
	void x(float x) { x_ = x; }
	void y(float y) { y_ = y; }
	void w(float w) { w_ = w; }
	void h(float h) { h_ = h; }

private:
	float x_;
	float y_;
	float w_;
	float h_;

};

float Distance(Rect r1, Rect r2);

#endif
