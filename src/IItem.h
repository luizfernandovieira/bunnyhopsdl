#ifndef IITEM_H
#define IITEM_H

#include "GameObject.h"

class IItem : public GameObject {
  public:
    IItem() {};
    virtual ~IItem() {};
    virtual void update(float dt) = 0;
    virtual void render() = 0;
    virtual void notifyCollision(GameObject &object) = 0;
    virtual bool is(const std::string &type) = 0;
    virtual bool isDead() = 0;
};

#endif
