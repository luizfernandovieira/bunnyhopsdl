#include "Game.h"
#include "Resources.h"
#include "InputHandler.h"
#include "Text.h"

Game* Game::instance_;

Game::Game(const char* name, int width, int height, int fullscreen) : stateStack_() {
	dt_ = 0.0;
	frameStart_ = 0;

	if (instance_ != nullptr) {
		std::cerr << "Another instance of the game is already running!" << std::endl;
		exit(1);
	}

	instance_ = this;

	initSDL();
	initWindow(name, width, height, fullscreen);
	initRenderer();
	initControllers();
	initAudio();
	initTTF();

	storedState_ = nullptr;
}

Game::~Game() {
	delete storedState_;

  stateStack_.empty();

  IMG_Quit();
	TTF_Quit();
	Mix_Quit();
  Mix_CloseAudio();
	SDL_DestroyRenderer(renderer_);
	SDL_DestroyWindow(window_);
	SDL_Quit();

	instance_ = nullptr;
}

Game& Game::getInstance() {
	return *instance_;
}

void Game::push(State* state) {
	storedState_ = state;
}

State& Game::getCurrentState() {
	return *stateStack_.top();
}

SDL_Renderer* Game::getRenderer() {
	if (SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0") == SDL_FALSE) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR ,
      "SCALE QUALITY" , "Could not set linear scaling quality", NULL);
  }
	return renderer_;
}

float Game::getDeltaTime() {
	return dt_;
}

void Game::calculateDeltaTime() {
	int lastFrameStart = frameStart_;
	frameStart_ = SDL_GetTicks();
	dt_ = (float)(frameStart_ - lastFrameStart) / 1000.0;
	if (dt_ > 0.1) dt_ = 0.1;
}

void Game::run() {
	if (storedState_ != nullptr) {
		stateStack_.emplace(storedState_);
		storedState_ = nullptr;
	}

	while(!stateStack_.empty() && !stateStack_.top()->quitRequested()) {
		calculateDeltaTime();

		stateStack_.top()->update(dt_);
		stateStack_.top()->render();
		SDL_RenderPresent(renderer_);

		if (stateStack_.top()->popRequested()) {
			stateStack_.pop();
			Resources::clearImages();
      Resources::clearMusics();
			Resources::clearSounds();
			Resources::clearFonts();
			stateStack_.top()->resume();
		}

    if (storedState_ != nullptr) {
			stateStack_.top()->pause();
			stateStack_.emplace(storedState_);
			storedState_ = nullptr;
		}

		auto frameEnd = (float) SDL_GetTicks();

		if((frameEnd-frameStart_) < (float)(1000.f / 60.0)) {
      while(frameEnd - frameStart_ <= (1000.f / 60.0)) {
				frameEnd = (float)SDL_GetTicks();
      }
    }
	}

	while (!stateStack_.empty()) {
		stateStack_.pop();
  }

	Resources::clearImages();
  Resources::clearMusics();
	Resources::clearSounds();
	Resources::clearFonts();
}

void Game::initSDL() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		exit(1);
	}
}

void Game::initWindow(const char* name, int width, int height, int fullscreen) {
	SDL_DisplayMode mode;
	if (SDL_GetDesktopDisplayMode(0, &mode) != 0) {
		std::cerr << "Error creating the game window" << std::endl <<
      SDL_GetError() << std::endl;
		exit(1);
	}

	window_ = SDL_CreateWindow(
    name,
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width,
    height,
    SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
	);

	if (window_ == nullptr) {
		std::cerr << "Error creating the game window" << std::endl <<
      SDL_GetError() << std::endl;
		exit(1);
	}

  if(fullscreen) {
    SDL_SetWindowFullscreen(window_, SDL_WINDOW_FULLSCREEN_DESKTOP);
  }

  updateResolution();
}

void Game::initRenderer() {
	renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);
	if (renderer_ == nullptr) {
		std::cerr << "Error initializing game renderer" << std::endl <<
      SDL_GetError() << std::endl;
		exit(1);
	}
}

void Game::initControllers() {
  int maxJoystickCount = SDL_NumJoysticks();
  if(maxJoystickCount > 1) maxJoystickCount = 1;
	for(int i=0; i<maxJoystickCount; i++) {
		if(SDL_IsGameController(i)) {
			printf("Index \'%i\' is a compatible controller, named \'%s\'\n", i,
        SDL_GameControllerNameForIndex(i));

      controller_ = SDL_GameControllerOpen(i);
      SDL_Joystick* joystrick = SDL_GameControllerGetJoystick(controller_);
      InputHandler::getInstance().setControllerInstanceId(SDL_JoystickInstanceID(joystrick));

			printf("Controller %i is mapped as \"%s\".\n", i,
        SDL_GameControllerMapping(controller_));
		} else {
			printf("Index \'%i\' is not a compatible controller.", i);
		}
	}
}

void Game::initAudio(){
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024)){
		printf( "SDL_mixer could not Open Audio! SDL_mixer Error: %s\n", Mix_GetError());
		system("PAUSE");
		exit(1);
	}

	if (Mix_Init(MIX_INIT_OGG) != MIX_INIT_OGG){
		printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		system("PAUSE");
		exit(1);
	}
}

void Game::initTTF() {
	if ( TTF_Init() != 0 )
	{
		std::cerr << "SDL_ttf could not initialize! SDL_ttf Error: " <<
      TTF_GetError() << std::endl;
		exit(1);
	}
}

void Game::changeResolution(int width, int height) {
  SDL_SetWindowSize(window_ , width, height);
	updateResolution();
}

void Game::setWindowBordered(bool on) {
  SDL_SetWindowBordered(window_, on ? SDL_TRUE : SDL_FALSE);
}

void Game::setFullscreen(bool on) {
	SDL_SetWindowFullscreen(window_, on ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
	updateResolution();
}

void Game::setVsync(bool on) {
  SDL_GL_SetSwapInterval(on ? 0 : 1);
}

void Game::updateResolution() {
  SDL_RenderClear(renderer_);
  SDL_RenderSetLogicalSize(renderer_, 160, 144);

  SDL_GetWindowSize(window_, &width_, &height_);
  SDL_SetWindowSize(window_, width_, height_);

  printf("W: %d, H: %d\n", width_, height_);

  double originalRatio = 160.0 / 144;
  double newRatio = (1.0 * width_) / height_;
  double offsetX, offsetY, w, h;

  if(originalRatio < newRatio) {
    h = height_;
    w = height_ * originalRatio;
    offsetY = 0;
    offsetX =  (144 / h) * (w - width_) / 2;
  } else {
    w = width_;
    h = width_ / originalRatio;
    offsetX = 0;
    offsetY = (160 / w) * (h - height_) / 2;
  }

  SDL_SetWindowPosition(window_, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);

  // InputManager::get_instance()->set_mouse_scale(1280 / w, offset_x, offset_y);
}
