#include "Princess.h"
#include "Camera.h"

Princess::Princess(int x, int y) :
    sp_("../img/entity/princess/princess.png", 1, 0.1) {
  box_.x(x);
  box_.y(y);
  box_.w(sp_.getWidth());
  box_.h(sp_.getHeight());
}

Princess::~Princess() {

}

void Princess::update(float dt) {

}

void Princess::render() {
  sp_.render((int) (box_.x() - Camera::pos_.x()), (int) (box_.y() - Camera::pos_.y()));
}

void Princess::notifyCollision(GameObject& object) {

}

bool Princess::is(const std::string& type) {
  return false;
}

bool Princess::isDead() {
  return false;
}