#ifndef BULLET_PLAYER_SHOTGUN_H
#define BULLET_PLAYER_SHOTGUN_H

#include "Bullet.h"
#include "Sprite.h"

class BulletPlayerShotgun : public Bullet {

public:
	BulletPlayerShotgun(int x = 0, int y = 0);
	~BulletPlayerShotgun();
	void update(float dt);
	void render();
  void notifyCollision(GameObject & object);
  bool is(const std::string& type);
  bool isDead();

private:
  Sprite sp_;
  int moveSpeed_;

};

#endif
