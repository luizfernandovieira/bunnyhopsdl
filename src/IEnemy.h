#ifndef ENEMY_H
#define ENEMY_H

#include "GameObject.h"
#include "Timer.h"

class IEnemy : public GameObject {

public:
	IEnemy() :
    moveUp_(0),
    moveDown_(0),
    moveRight_(0),
    moveLeft_(0),
    moveHorizontal_(0),
    moveVertical_(0),
    moveSpeed_(50),
    gravity_(0),
    facingRight_(false),
    life_(1),
    takingDmg_(false),
    canTakeDmg_(true),
    takeDmgCooldown_(0.5),
    takeDmgTimer_() {};
	virtual ~IEnemy() {};
	virtual void update(float dt) = 0;
	virtual void render() = 0;
  virtual void notifyCollision(GameObject & object) = 0;
  virtual bool is(const std::string& type) = 0;
  virtual bool isDead() = 0;

protected:
  int moveUp_;
  int moveDown_;
  int moveRight_;
  int moveLeft_;
  int moveHorizontal_;
  int moveVertical_;
  int moveSpeed_;
  int gravity_;
  bool facingRight_;
  int life_;
  bool takingDmg_;
  bool canTakeDmg_;
  float takeDmgCooldown_;
  Timer takeDmgTimer_;

};

#endif
