#include "Vec.h"

Vec::Vec() {
	x_ = 0.0;
	y_ = 0.0;
}

Vec::Vec(float x, float y) {
	x_ = x;
	y_ = y;
}

Vec Vec::operator+ (const Vec& v) {
	Vec vec;
	vec.x(x_ + v.x());
	vec.y(y_ + v.y());
	return vec;
}

Vec Vec::operator- (const Vec& v) {
	Vec vec;
	vec.x(x_ - v.x());
	vec.y(y_ - v.y());
	return vec;
}

Vec& Vec::operator+= (const Vec& v) {
	x_ += v.x();
	y_ += v.y();
	return (*this);
}

Vec& Vec::operator-= (const Vec& v) {
	x_ -= v.x();
	y_ -= v.y();
	return (*this);
}

Vec Vec::operator+ (const float& e) const {
	Vec vec;
	vec.x(x_ + e);
	vec.y(y_ + e);
	return vec;
}

Vec Vec::operator- (const float& e) const {
	Vec vec;
	vec.x(x_ - e);
	vec.y(y_ - e);
	return vec;
}

Vec Vec::operator* (const float& e) const {
	Vec vec;
	vec.x(x_ * e);
	vec.y(y_ * e);
	return vec;
}

bool Vec::operator== (const Vec& v) const {
	if(x_ == v.x() && y_ == v.y()) {
    return true;
  } else {
    return false;
  }
}

void Vec::rotate(float angle) {
	angle = (float) (angle * PI / 180);
	float curX = x_;
	float curY = y_;
	x_ = curX * cos(angle) - curY * sin(angle);
	y_ = curY * cos(angle) + curX * sin(angle);
}

void Vec::rotate(Vec vec, float module, float angle) {
	angle = (float) (angle * PI / 180);
	x_ = module * sin(angle) + vec.x();
	y_ = module * cos(angle) + vec.y();
}

float Distance(Vec v1, Vec v2) {
	return (float) (sqrt( pow(v2.x() - v1.x(), 2.0) + pow(v2.y() - v1.y(), 2.0) ));
}

float LineInclination(Vec v1, Vec v2) {
	float angle = (float) (atan((v2.y() - v1.y()) / (v2.x() - v1.x())) * 180 / PI);
	if (v1.x() > v2.x())
		return angle + 180;
	return angle;
}

float Magnitude(const Vec& v) {
	return std::sqrt(v.x() * v.x() + v.y() * v.y());
}

Vec Normalize(const Vec& v) {
	return v * (1.f / Magnitude(v));
}

float Dot(const Vec& a, const Vec& b) {
	return a.x() * b.x() + a.y() * b.y();
}

float ProjectX(float module, float angle) {
	angle = (float) (angle * PI / 180);
	return cos(angle) * module;
}

float ProjectY(float module, float angle) {
	angle = (float) (angle * PI / 180);
	return sin(angle) * module;
}
